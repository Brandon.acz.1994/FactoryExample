/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DocumentReaders;

import DocumentsConverters.TXTConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Brandon
 */
public class TXTReader extends DocumentReader{

    private final String input;

    public TXTReader(String input) {
        super(input);
        this.input=input;
    }

  
    
    @Override
    public List<String> Read(){
    
         try {
             Stream<String> lines= Files.lines(Paths.get(input));
             List<String> templist = lines.collect(Collectors.toList()); 
             return templist;
         } catch (IOException ex) {
             Logger.getLogger(TXTConverter.class.getName()).log(Level.SEVERE, null, ex);
         }
             
     return null;
     }
    
}
