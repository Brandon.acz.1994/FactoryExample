/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryexample;

import ConverterFactory.DocumentConverterFactory;
import DocumentsConverters.DocumentConverter;
import ConverterFactory.ConverterFactoryMethod;
import StudentDatabase.*;

import java.io.IOException;

/**
 *
 * @author Brandon
 */
public class FactoryExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Student brandon = new Student();
        brandon.setId(12170734);
        brandon.setName("brandon");
        brandon.setFatherlastname("cardenas");
        brandon.setMotherlastname("sainz");
        brandon.setAge(23);   
        brandon.setSex("male");               
        brandon.setCareer("mecatronics");
        brandon.setSemester(8); 
        
        Student emanuel = new Student();
        emanuel.setId(12170735);
        emanuel.setName("emanuel");
        emanuel.setFatherlastname("gutierrez");
        emanuel.setMotherlastname("artiza");
        emanuel.setAge(22);   
        emanuel.setSex("male");               
        emanuel.setCareer("sistems");
        emanuel.setSemester(5); 
        
        Student hector = new Student();
        hector.setId(12170755);
        hector.setName("hector");
        hector.setFatherlastname("cardenas");
        hector.setMotherlastname("lopez");
        hector.setAge(26);   
        hector.setSex("male");               
        hector.setCareer("mecatronics");
        hector.setSemester(5); 
        
        Student pako = new Student();
        pako.setId(12370730);
        pako.setName("francisco");
        pako.setFatherlastname("hernandez");
        pako.setMotherlastname("hernandez");
        pako.setAge(30);   
        pako.setSex("male");               
        pako.setCareer("sistems");
        pako.setSemester(8); 
        
        
        Scholar database = new Scholar();
        database.add(brandon);
        database.add(emanuel);
        database.add(hector);
        database.add(pako);
        
        database.toTXT("liststudents.txt");
        
         ConverterFactoryMethod factory= new DocumentConverterFactory();
         
         DocumentConverter txtTOtxt = factory.createConverter("liststudents.txt", "toTXT.txt");
         txtTOtxt.converting();
         DocumentConverter txtTOcsv = factory.createConverter("liststudents.txt", "toCSV.csv");
         txtTOcsv.converting();
         DocumentConverter txtTOjson = factory.createConverter("liststudents.txt", "toJSON.json");
         txtTOjson.converting();
         DocumentConverter txtTOxml = factory.createConverter("liststudents.txt", "toXML.xml");
         txtTOxml.converting();
         
         
        
        
        
    }
    
}
