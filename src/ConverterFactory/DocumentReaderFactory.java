/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConverterFactory;

import DocumentReaders.DocumentReader;
import DocumentReaders.TXTReader;

/**
 *
 * @author Brandon
 */
public class DocumentReaderFactory implements ReaderFactoryMethod {

    @Override
    public DocumentReader createReader(String input) {
      
        if(input.endsWith(".txt")){         
            
            System.out.println("archivo .txt detectado");
            
            
            return new TXTReader(input);   
        }
    System.out.println("archivo no reconocible");
    return null;
        
    }
    
}
