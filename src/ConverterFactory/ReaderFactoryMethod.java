/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConverterFactory;

import DocumentReaders.DocumentReader;

/**
 *
 * @author Brandon
 */
public interface ReaderFactoryMethod {

    public DocumentReader createReader(String input);
    
}
