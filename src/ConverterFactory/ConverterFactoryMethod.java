/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConverterFactory;

import DocumentsConverters.DocumentConverter;

/**
 *
 * @author Brandon
 */
public interface ConverterFactoryMethod {
    
    public DocumentConverter createConverter(String input,String output);
    
    
    
    
}
