/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConverterFactory;


import DocumentReaders.DocumentReader;
import DocumentsConverters.CSVConverter;
import DocumentsConverters.DocumentConverter;
import DocumentsConverters.JSONConverter;
import DocumentsConverters.XMLConverter;
import DocumentsConverters.TXTConverter;


/**
 *
 * @author Brandon
 */
public class DocumentConverterFactory implements ConverterFactoryMethod {

    @Override
    public DocumentConverter createConverter(String input, String output) {
                
        ReaderFactoryMethod factory= new DocumentReaderFactory();
        DocumentReader reader = factory.createReader(input);
              
         if(output.endsWith(".csv")){
            
            return new CSVConverter(output,reader.Read());    
        }
        
        if(output.endsWith(".txt")){            
            
            return new TXTConverter(output,reader.Read());
        }
        
        if(output.endsWith(".xml")){
            
        return new XMLConverter(output,reader.Read());
        }
        
        if(output.endsWith(".json")){
        
            return new JSONConverter(output,reader.Read());
        }
        System.out.println("formato no reconocible");
        return null;
    
       
    }
    
         
    
}
