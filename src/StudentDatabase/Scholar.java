/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentDatabase;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Brandon
 */
public class Scholar {
    
    private List<Student> database= new ArrayList();
    
    
    public void toTXT(String out) throws FileNotFoundException, IOException{
    File fout = new File(out);
       
            FileOutputStream fos = new FileOutputStream(fout);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            
            bw.write("NoDeControl|Nombre|ApellidoPaterno|ApellidoMaterno|Edad|Sexo|Carrera|Semestre");
            bw.newLine();
            
            database.stream().forEach(student -> { 
                                
        try {
            
            bw.write(student.toString());
            bw.newLine();
            
            
        } catch (IOException ex) {
            Logger.getLogger(Scholar.class.getName()).log(Level.SEVERE, null, ex);
        }
               
            
            });
           System.out.println("base de datos convertido a texto\n"); 
           bw.close(); 
       
    
    
    }

    public void add(Student brandon) {
       database.add(brandon);
    }
}
