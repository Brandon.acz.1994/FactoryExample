/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentDatabase;

/**
 *
 * @author Brandon
 */
public class Student {
 private int id; 
 private String name;
 private String fatherlastname; 
 private String motherlastname;  
 private int age;
 private String sex;
 private String career;
 private int semester;

 
  public Student(int id,String name,String flastname,String mlastname,
          int age, String sex,
          String career, int semester){
      this.id=id;
      this.name=name;
      this.fatherlastname=flastname;
      this.motherlastname=mlastname;
      this.age=age;
      this.sex=sex;
      this.career=career;
      this.semester=semester;
      
 
 }

    public Student() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   
    public String getFatherlastname() {
        return fatherlastname;
    }

    public void setFatherlastname(String fatherlastname) {
        this.fatherlastname = fatherlastname;
    }

    public String getMotherlastname() {
        return motherlastname;
    }

    public void setMotherlastname(String motherlastname) {
        this.motherlastname = motherlastname;
    }
    
    

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }
    
 @Override
    public String toString(){
     return id+"|"+name+"|"+fatherlastname+"|"+motherlastname+"|"+age+"|"+sex+"|"+career+"|"+semester;
    
    }
  
 
    
}
