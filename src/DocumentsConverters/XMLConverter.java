/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DocumentsConverters;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 *
 * @author Brandon
 */
public class XMLConverter extends DocumentConverter {

    private final List<String> templist;
    List<List<String> > templistelements= new ArrayList();
    private final String output;

    public XMLConverter(String output, List<String> templist) {
        super(output,templist);
        this.templist=templist;
        this.output=output;
    }

   
    
    
    @Override
    public void converting(){
    System.out.println("convirtiendo a .xml ");

    
        String classes = templist.get(0).replace("|", "\u0009");        
        List<String> classeslist = Arrays.asList(classes.split("\u0009"));
        templist.remove(0);  
        
        templist.stream().forEach(p ->{
                    
            String replaced = p.replace("|", "\u0009");
        List<String> items = Arrays.asList(replaced.split("\u0009"));
        
        templistelements.add(items);
        });    
        
           
                     
        try {
            DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
            DocumentBuilder build = dFact.newDocumentBuilder();
            Document doc = build.newDocument();
            
              Element root = doc.createElement("Studentinfo");
             doc.appendChild(root);

            
             
             
             templistelements.stream().forEach(list -> {
             Element Details = doc.createElement("Details");
             root.appendChild(Details);    
                 
             
             for(int i=0; i<list.size();i++){
                 
                            
                Element name = doc.createElement(classeslist.get(i));
                name.appendChild(doc.createTextNode(String.valueOf(list.get(i))));
                Details.appendChild(name);
          
            }
                 
             
        });
             
                        
        TransformerFactory tranFactory = TransformerFactory.newInstance();
        Transformer aTransformer = tranFactory.newTransformer();      
        DOMSource source = new DOMSource(doc);
	StreamResult result = new StreamResult(new File(output));
      
        aTransformer.transform(source, result);

		  
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XMLConverter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(XMLConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    System.out.println("Guardado!\n");
    }
    
    
}
