/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DocumentsConverters;



import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;


/**
 *
 * @author Brandon
 */
public class JSONConverter extends DocumentConverter {

    private final List<String> templist;
    private final List<List<String> > templistelements= new ArrayList();
    private final String output;

    public JSONConverter(String output, List<String> templist) {
         super(output,templist);
        this.templist=templist;
        this.output=output;
    }

    
        
    @Override
    public void converting(){
    System.out.println("convirtiendo a .json ");    
        
     String classes = templist.get(0).replace("|", "\u0009");        
        List<String> classeslist = Arrays.asList(classes.split("\u0009"));
        templist.remove(0);  
        
     templist.stream().forEach(p ->{
        
            
            String replaced = p.replace("|", "\u0009");
        List<String> items = Arrays.asList(replaced.split("\u0009"));
        
        templistelements.add(items);
        });    
            
  
        try {
            PrintWriter outs = new PrintWriter(new BufferedWriter(new FileWriter(output)));
            
              templistelements.stream().forEach(list->{
                  
        JSONObject obj = new JSONObject();
        for(int i=0; i<list.size(); i++){            
       
        obj.put(classeslist.get(i),list.get(i));
        
        }
        outs.println(obj.toString());        
        
    });              
            
       outs.close();     
            
        } catch (IOException ex) {
            Logger.getLogger(JSONConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
     System.out.println("Guardado!\n");  
    } 
   
}
