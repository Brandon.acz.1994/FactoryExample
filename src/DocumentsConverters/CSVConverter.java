/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DocumentsConverters;



import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Brandon
 */
public class CSVConverter extends DocumentConverter {

    private final List<String> templist;
    private final String output;

    public CSVConverter(String output, List<String> templist) {
        super(output,templist);
        this.templist=templist;
        this.output=output;
    }

  

   
    
    @Override
    public void converting(){
    System.out.println("convirtiendo a .csv ");
        try {
            PrintWriter outs = new PrintWriter(new BufferedWriter(new FileWriter(output)));
               templist.stream().forEach(p -> { 
                String temp = p.replace("|", ",");
            outs.println(temp);
            
              
            });
               
            outs.close();
            
        } catch (IOException ex) {
            Logger.getLogger(CSVConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
    System.out.println("Guardado!\n");    
    }
    

    
}
